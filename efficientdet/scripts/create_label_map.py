import glob
import json
import argparse
import os
from tqdm import tqdm


LABELS = [
    "A",
    "B",
    "C",
    "D",
    "del",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "space",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
]
LABELS_CODE = {label: label_code + 1 for label_code, label in enumerate(LABELS)}
LABELS_CODE_INDEX = {(label_code +1): label for label_code, label in enumerate(LABELS)}

def write_label_map(dataset_path, label_index_file):
    labels = json.load(label_index_file)
    label_map_path = os.path.join(dataset_path, 'annotations', 'label_map.pbtxt')
    with open(label_map_path,'w') as f:
        for label_index, label in labels.items():
            f.write("item {id: " + str(label_index) + ", name: '" + str(label) + "'}\n")
    print('Created label map dumped at {}'.format(label_map_path))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-p",
        "--path",
        type=str,
        help="path to dataset for creating label map",
    )
    parser.add_argument(
        "-l",
        "--label",
        type=str,
        help="path to label index json file",
    )
    
    args = vars(parser.parse_args())
    print("Creating label map from dataset at {}, using label index JSON file at {}".format(args['path'],args['label']))
    write_label_map(args['path'], args['label'])