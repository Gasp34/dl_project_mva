import glob
import json
import argparse
import os
from tqdm import tqdm
WIDTH = 512
HEIGHT= 512



LABELS = [
    "A",
    "B",
    "C",
    "D",
    "del",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "space",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
]
LABELS_CODE = {label: label_code + 1 for label_code, label in enumerate(LABELS)}
LABELS_CODE_INDEX = {(label_code + 1): label for label_code, label in enumerate(LABELS)}


def convert_split_yolo2coco(path_split, image_id_count, annotation_id_count):
    yolo_txt_split_filepaths = glob.glob(path_split + '/*.txt')
    yolo_txt_split_filepaths.sort()
    annotations_split_dict = {}
    annotations_split_dict["info"] = {
        "description": "ASL + ASL Test + Roboflow  Dataset",
    }
    annotations_split_dict['licenses'] = [
        {
            "url": "",
            "id": 0,
            "name": "ASL dataset"
        },
        {
            "url": "",
            "id": 1,
            "name": "ASL Test dataset"
        },
        {
            "url": "",
            "id": 2,
            "name": "Roboflow dataset"
        },
    ]
    annotations_split_dict['categories'] = [
        {
            "supercategory": "hand",
            "id": label_index + 1,
            "name": label,
            } for label_index, label in enumerate(LABELS)
    ]
    images = []
    annotations = []
    for yolo_txt_filepath in tqdm(yolo_txt_split_filepaths, total=len(yolo_txt_split_filepaths)):
        image = {
            "licence":0,
            "file_name": yolo_txt_filepath.split('/')[-1][:-4] + ".jpg",
            "height": HEIGHT,
            "width": WIDTH,
            "id":  image_id_count,
            }
        images.append(image)
        with open(yolo_txt_filepath,'r') as yolo_file:
            label, x_centered, y_centered, width, height = yolo_file.readlines()[0].split(' ')
            x_centered = float(x_centered)
            y_centered = float(y_centered)
            width = float(width)
            height = float(height)
    
        x1 = int((x_centered-width/2)*WIDTH)
        y1 = int((y_centered-height/2)*HEIGHT)
        x2 = int((x_centered+width/2)*WIDTH)
        y2 = int((y_centered+height/2)*HEIGHT)
        bb_width = min(int(width*WIDTH), WIDTH - x1)
        bb_height = min(int(height*HEIGHT), HEIGHT - y1)

        annotation = {
            "image_id": image_id_count,
            "segmentation": [
                x1,
                y1,
                x2,
                y2,
                ],
            "area": (x2-x1)*(y2-y1),
            "bbox": [
                x1,
                y1,
                bb_width,
                bb_height,
                ],
            "category_id": str(int(label) + 1),
            "id": annotation_id_count,
            "iscrowd": 0,
        }
        annotations.append(annotation)
        image_id_count += 1
        annotation_id_count += 1
    annotations_split_dict["images"] = images
    annotations_split_dict["annotations"] = annotations
    print('Found {} image files.'.format(len(images)))
    print('Found {} annotation files.'.format(len(annotations)))
    annotation_path = '/'.join(path_split.split('/')[:-1]) + "annotations/instances_{}.json".format(path_split.split('/')[-1])
    print('Dump COCO JSON annotations at {}.'.format(annotation_path))
    with open(annotation_path, 'w') as coco_file:
        json.dump(annotations_split_dict, coco_file, indent=4)
    return image_id_count, annotation_id_count


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-p",
        "--path",
        type=str,
        help="path to dataset to convert from YOLO txt to COCO JSON",
    )
    parser.add_argument(
        "-s",
        "--splits",
        type=str,
        help="splits to dataset to convert from YOLO txt to COCO JSON",
        nargs="+"
    )
    args = vars(parser.parse_args())
    if not os.path.isdir(args['path']  + 'annotations/'):
        os.makedirs(args['path']  + 'annotations/')
    image_id_count, annotation_id_count = 0, 0
    for split in args['splits']:
        print("Converting {} annotations...".format(split))
        image_id_count, annotation_id_count = convert_split_yolo2coco(args['path'] + "/" + split, image_id_count, annotation_id_count)
    print("Done")