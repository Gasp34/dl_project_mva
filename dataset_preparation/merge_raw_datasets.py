import os

import json
from shutil import copyfile
import pandas as pd
import argparse
import numpy as np
from collections import defaultdict


MERGED_FOLDER_DATA_PATH = "./data/processed/merged_dataset/data"
MERGED_FOLDER_LABEL_PATH = "./data/processed/merged_dataset/labels"

ASL_DATASET_PATH = "./data/asl-alphabet"
ASLTEST_DATASET_PATH = "./data/asl-alphabet-test"
ROBOFLOW_DATASET_PATH = "./data/ASL_Roboflow"

ROBOFLOW_WIDTH = 1536
ROBOFLOW_HEIGTH = 2047

LABELS = [
    "A",
    "B",
    "C",
    "D",
    "del",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "space",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
]
LABELS_CODE = {label: label_code + 1 for label_code, label in enumerate(LABELS)}
LABELS_CODE_INDEX = {(label_code + 1): label for label_code, label in enumerate(LABELS)}

DATASET_INDEX = {
    "asl-alphabet": "ASL",
    "asl-alphabet-test": "ASLTest",
    "ASL_Roboflow": "Roboflow",
}


def load_data_paths(DATASET_PATH, dataset_index):
    paths_per_label = defaultdict(list)
    for (dirpath, _, filenames) in os.walk(DATASET_PATH):
        if dataset_index != "Roboflow":
            label = dirpath.split("/")[-1]
            if label in LABELS:
                paths_per_label[label] += [os.path.join(dirpath, file) for file in filenames]
        else:
            for filename in filenames:
                label = filename[0]
                if label in LABELS:
                    paths_per_label[label] += [os.path.join(dirpath, filename)]
    for label in paths_per_label.keys():
        paths_per_label[label] = set(paths_per_label[label])
    print({label: len(paths_per_label[label]) for label in paths_per_label.keys()})
    return paths_per_label


def merge_datasets(paths_per_label_list):
    # Roboflow labels
    df_roboflow = pd.concat(
        [pd.read_csv(os.path.join(ROBOFLOW_DATASET_PATH, csv_file), header=None) for csv_file in ["train.csv", "test.csv", "valid.csv"]],
        axis=0,
    )
    df_roboflow.columns = ["file", "x1", "x2", "y1", "y2", "label"]
    for paths_per_label in paths_per_label_list:
        labels_count = {label: 0 for label in LABELS}
        for label, file_paths in paths_per_label.items():
            for file_path in file_paths:
                dataset_label = DATASET_INDEX[file_path.split("/")[2]]
                if dataset_label == "Roboflow":
                    if file_path.split("/")[-2] != "no_bg":
                        file_labels = df_roboflow[df_roboflow.file == "/".join(file_path.split("/")[-2:])].values[0]
                        copyfile(
                            file_path,
                            os.path.join(
                                MERGED_FOLDER_DATA_PATH,
                                dataset_label + "_" + label + "_" + str(labels_count[label]) + ".jpg",
                            ),
                        )
                        x_center = (file_labels[3] + file_labels[1]) / 2 / ROBOFLOW_WIDTH
                        y_center = (file_labels[4] + file_labels[2]) / 2 / ROBOFLOW_HEIGTH
                        width = (file_labels[3] - file_labels[1]) / ROBOFLOW_WIDTH
                        height = (file_labels[4] - file_labels[2]) / ROBOFLOW_HEIGTH
                        with open(
                            os.path.join(
                                MERGED_FOLDER_LABEL_PATH,
                                dataset_label + "_" + label + "_" + str(labels_count[label]) + ".txt",
                            ),
                            "w",
                        ) as fp:
                            fp.write(
                                "{} {:.6f} {:.6f} {:.6f} {:.6f}".format(
                                    LABELS_CODE[file_labels[-1]],
                                    x_center,
                                    y_center,
                                    width,
                                    height,
                                )
                            )
                        labels_count[label] += 1
                else:
                    copyfile(
                        file_path,
                        os.path.join(
                            MERGED_FOLDER_DATA_PATH,
                            dataset_label + "_" + label + "_" + str(labels_count[label]) + ".jpg",
                        ),
                    )
                    with open(
                        os.path.join(
                            MERGED_FOLDER_LABEL_PATH,
                            dataset_label + "_" + label + "_" + str(labels_count[label]) + ".txt",
                        ),
                        "w",
                    ) as fp:
                        pass
                    labels_count[label] += 1


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-p",
        "--paths",
        type=str,
        help="paths to datasets to merge",
        nargs="+",
    )
    args = vars(parser.parse_args())

    # Make directories for merged dataset if necessary
    if not os.path.isdir(MERGED_FOLDER_DATA_PATH):
        os.makedirs(MERGED_FOLDER_DATA_PATH)

    if not os.path.isdir(MERGED_FOLDER_LABEL_PATH):
        os.makedirs(MERGED_FOLDER_LABEL_PATH)

    # Dump labels code index into json file
    with open(os.path.join(MERGED_FOLDER_LABEL_PATH, "labels_index.json"), "w") as outfile:
        json.dump(LABELS_CODE_INDEX, outfile, indent=4)

    # Load data images paths per label for all datasets
    paths_per_label_list = []
    for dataset_path in args["paths"]:
        print("Processing dataset at {}".format(dataset_path))
        paths_per_label_list.append(load_data_paths(dataset_path, DATASET_INDEX[dataset_path.split("/")[-1]]))

    # Merge datasets images into 1 folder with their labels
    merge_datasets(paths_per_label_list)
