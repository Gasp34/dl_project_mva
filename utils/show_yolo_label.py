#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  5 15:46:14 2020

@author: gaspard
"""
import cv2
import os
import random
import argparse
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-i","--image",type=str, help = "image folder")
    parser.add_argument("-l","--label",type=str, help = "labels forlder")
    args = parser.parse_args()
    
    leftkeys = (81, 110, 65361, 2424832)
    rightkeys = (83, 109, 65363, 2555904)
    
    image_folder = args.image
    label_folder = args.label
    
    if label_folder == None:
        label_folder = image_folder
    
    images = [x for x in os.listdir(image_folder) if x[-3:]=="jpg"]
    random.shuffle(images)
    i=0
    
    while True:
        image = images[i]
        image_path = image_folder+image
        label_path = label_folder+image[:-3]+"txt"
    
        f = open(label_path)
        
        lines = f.readlines()
        image = cv2.imread(image_path)
        
        if lines!=[]:
            box = lines[0].split(" ")
            
            x,y,w,h = float(box[1]), float(box[2]), float(box[3]), float(box[4])
            
            width,height = image.shape[1], image.shape[0]
            pt1 = (int((x-w/2)*width),int((y-h/2)*height))
            pt2 = (int((x+w/2)*width),int((y+h/2)*height))
            
            color = (255, 0, 0) 
            thickness = 2
            
            image = cv2.rectangle(image, pt1, pt2, color, thickness)
            
        else:
            print(f"No label : {label_path}")
    
        cv2.imshow('Image', image)
        key = cv2.waitKeyEx()
        
        if key in rightkeys:
            i = (i + 1) % len(images)
        if key in leftkeys:
            i -= 1
            if i < 0:
                i = len(images) - 1
    
        if (key == ord('q')) or (key == 27):
            break