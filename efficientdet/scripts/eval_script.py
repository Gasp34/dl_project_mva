import argparse
import os
import re
import tarfile

import wget

ROOT_FOLDER = os.getcwd()

##change chosen model to deploy different models available in the TF2 object detection zoo
MODELS_CONFIG = {
    "efficientdet-d0": {
        "model_name": "efficientdet_d0_coco17_tpu-32",
        "base_pipeline_file": "ssd_efficientdet_d0_512x512_coco17_tpu-8.config",
        "pretrained_checkpoint": "efficientdet_d0_coco17_tpu-32.tar.gz",
        "batch_size": 16,
    }
}

# in this tutorial we implement the lightweight, smallest state of the art efficientdet model
chosen_model = "efficientdet-d0"

model_name = MODELS_CONFIG[chosen_model]["model_name"]

DEPLOY_FOLDER = os.path.join(ROOT_FOLDER, os.path.join("efficientdet", "deploy"))
MODELS_FOLDER = os.path.join(DEPLOY_FOLDER, "models")
MODEL_FOLDER = os.path.join(MODELS_FOLDER, model_name)
# CONFIG FILE
custom_pipeline_config_path = os.path.join(MODEL_FOLDER, "pipeline_file.config")


if __name__ == "__main__":

    # DEPLOY FOLDER
    os.chdir(DEPLOY_FOLDER)
    # Write custom pipeline config file
    OUTPUT_FOLDER = os.path.join(DEPLOY_FOLDER, model_name + "-output")
    
    os.system(
                "python model_main_tf2.py --alsologtostderr --model_dir={} --pipeline_config_path={} --checkpoint_dir={} 2>&1 | tee {}/eval.log".format(
            OUTPUT_FOLDER, custom_pipeline_config_path, OUTPUT_FOLDER, OUTPUT_FOLDER
        )
    )