
# apt install 
# apt install -y protobuf-compiler

# Clone object detection repository
git clone --depth 1 https://github.com/tensorflow/models ./efficientdet/models

cd ./efficientdet/models/research
# Install Object detection API
protoc object_detection/protos/*.proto --python_out=.

# Build object detection API
cp object_detection/packages/tf2/setup.py ./
python3 -m pip install ./

# Test the installation.
#python3 object_detection/builders/model_builder_tf2_test.py

