#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  5 11:15:36 2020

@author: gaspard
"""

import glob
import cv2
import numpy as np
import tqdm
import os
import argparse
import re

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-f","--folder",type=str, help = "folder in which files must be resized")
    parser.add_argument("-s","--size",type=str, help = "target size, ex : 512x512", default="512x512")
    args = parser.parse_args()
    
    r = re.match("(.+)x(.+)",args.size)
    if r:
        size = (int(r.groups()[0]),int(r.groups()[1]))
        files = glob.glob(args.folder+"*.jpg")
        
        for filename in tqdm.tqdm(files):
            label = filename.replace("/data","/labels").replace("jpg","txt")
            f = open(label,"r")
            lines = f.readlines()
            f.close()
        
            if lines==[]:
                bbox = False
            else:
                bbox = True
            
            if bbox:
                box = lines[0].split(" ")
                cat,x,y,w,h =int(box[0]), float(box[1]), float(box[2]), float(box[3]), float(box[4])
            
            try:
                image = cv2.imread(filename)
                if image.shape != (512,512,3):
                    if "ASL" in filename: #ASL files are 200x200
                        image = image[3:-3,3:-3,:] #remove blue line
                        bg = np.zeros((size[0],size[1],3),dtype="uint8")
                        bg[159:159+194,159:159+194,:] = image
                        cv2.imwrite(filename,bg)
                        
                        if bbox:
                            x,y,w,h = (x*200-3+160)/512, (y*200-3+160)/512 , w*200/512, h*200/512
                        
                    elif "Roboflow" in filename: #Roboflow are 2047x1536
                        image = cv2.copyMakeBorder(image, 0, 0, 255, 256, cv2.BORDER_CONSTANT,value=[0, 0, 0])
                        image = cv2.resize(image,(512,512))
                        cv2.imwrite(filename,image)
                        
                        if bbox:
                            x = (x*1536+255)/2047
                            w = w*1536/2047
                            
                    f = open(label,"w")
                    if bbox:
                        l = str(cat)+ " "+" ".join([str(round(i,3)) for i in [x,y,w,h]])
                        f.write(l)
                    f.close()
            except:
                print(f"Fail : {filename}")
                os.remove(filename)

    else:
        print(f"Size {args.size} not understood")