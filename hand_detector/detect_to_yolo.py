#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  4 17:30:15 2020

@author: gaspard
"""

import argparse
import glob

from keras_retinanet import models
from keras_retinanet.utils.image import read_image_bgr, preprocess_image, resize_image

import numpy as np
import tqdm
import json
import re

def find_id(name,classes):
    for key in classes.keys():
        if classes[key]==name:
            return key

def find_box(model,filename, output,thresh): #display the detection and returns box,score,label
     # load image
     image = read_image_bgr(filename)
     width, height = image.shape[1], image.shape[0]
     
     # preprocess image for network
     image = preprocess_image(image)
     image, scale = resize_image(image,max_side=512)
     
     #prediction
     boxes, scores, labels = model.predict_on_batch(np.expand_dims(image, axis=0))

     # correct for image scale
     boxes /= scale

     # best box
     box, score = boxes[0][0],scores[0][0]
     
     file_split = filename.split("/")
     label_path = output + file_split[-1][:-3]+"txt" 
     
     r = re.match(".*/.*_(.*)_[0-9]+.jpg",filename)
     name = r.groups()[0]
     class_id = find_id(name,classes)
     
     f = open(label_path,"w")
     if score >= thresh:
         x1, y1, x2, y2  = box
         yolo_box = [(x2+x1)/(2*width), (y2+y1)/(2*height),(x2-x1)/(width), (y2-y1)/(height)]
         yolo_annot = [str(class_id)] + [str(round(x,3)) for x in yolo_box]
        
         f.write(" ".join(yolo_annot)+"\n")
     f.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-f","--files",type=str, help = "files to process")
    parser.add_argument("-o","---output", type=str,help = "folder in which labels are created")
    parser.add_argument("-m","--model",type=str, help = "model path", default="./hand_detector_inf.h5")
    parser.add_argument("-t","--threshold",type=float, help = "threshold value")
    args = parser.parse_args()
    
    j = open("../labels_index.json")
    classes = json.load(j)
    j.close()
    
    files = glob.glob(args.files)

    model = models.load_model(args.model, backbone_name='resnet50')
    
    for filename in tqdm.tqdm(files):
        find_box(model,filename,args.output,args.threshold)


