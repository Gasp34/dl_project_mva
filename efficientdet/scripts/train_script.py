import argparse
import os
import re
import tarfile

import wget
from object_detection.utils import label_map_util

ROOT_FOLDER = os.getcwd()

##change chosen model to deploy different models available in the TF2 object detection zoo
MODELS_CONFIG = {
    "efficientdet-d0": {
        "model_name": "efficientdet_d0_coco17_tpu-32",
        "base_pipeline_file": "ssd_efficientdet_d0_512x512_coco17_tpu-8.config",
        "pretrained_checkpoint": "efficientdet_d0_coco17_tpu-32.tar.gz",
        "batch_size": 16,
    }
}

# in this tutorial we implement the lightweight, smallest state of the art efficientdet model
chosen_model = "efficientdet-d0"

NUM_STEPS = 5000  # The more steps, the longer the training. Increase if your loss function is still decreasing and validation metrics are increasing.
WARMUP_STEPS = 200
LEARNING_RATE_BASE = 0.03
WARMUP_LEARNING_RATE = 0.001
BATCH_SIZE = 16
CHECKPOINT_INTERVAL = 500

model_name = MODELS_CONFIG[chosen_model]["model_name"]
pretrained_checkpoint = MODELS_CONFIG[chosen_model]["pretrained_checkpoint"]
base_pipeline_file = MODELS_CONFIG[chosen_model]["base_pipeline_file"]

DEPLOY_FOLDER = os.path.join(ROOT_FOLDER, os.path.join("efficientdet", "deploy"))
MODELS_FOLDER = os.path.join(DEPLOY_FOLDER, "models")
MODEL_FOLDER = os.path.join(MODELS_FOLDER, model_name)
# CONFIG FILE
download_config = "https://raw.githubusercontent.com/tensorflow/models/master/research/object_detection/configs/tf2/" + base_pipeline_file
base_pipeline_config_path = os.path.join(MODEL_FOLDER, base_pipeline_file)
# FINE-TUNE CHECKPOINT
FINETUNE_CHECKPOINT = os.path.join(MODEL_FOLDER, "/checkpoint/ckpt-0")

custom_pipeline_config_path = os.path.join(MODEL_FOLDER, "pipeline_file.config")


def get_num_classes(pbtxt_fname):
    label_map = label_map_util.load_labelmap(pbtxt_fname)
    categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=90, use_display_name=True)
    category_index = label_map_util.create_category_index(categories)
    return len(category_index.keys())


def write_custom_config_file(base_pipeline_config_path, custom_pipeline_config_path, dataset_name, test=True):
    train_record_fname = os.path.join(ROOT_FOLDER, "data", dataset_name, "tf_records", "coco_train.record*")
    val_record_fname = os.path.join(ROOT_FOLDER, "data", dataset_name, "tf_records", "coco_val.record*")
    test_record_fname = os.path.join(ROOT_FOLDER, "data", dataset_name, "tf_records", "coco_testdev.record*")
    label_map_path = os.path.join(ROOT_FOLDER, "data", dataset_name, "annotations", "label_map.pbtxt")

    with open(base_pipeline_config_path) as f:
        s = f.read()
    with open(custom_pipeline_config_path, "w") as f:
        # Set learning rate of classes num_classes.
        s = re.sub("warmup_steps: [0-9]+", "warmup_steps: {}".format(WARMUP_STEPS), s)
        s = re.sub('fine_tune_checkpoint: ".*?"', 'fine_tune_checkpoint: "{}"'.format(FINETUNE_CHECKPOINT), s)
        # tfrecord files train, eval and test.
        s = re.sub('(input_path: ".*?)(PATH_TO_BE_CONFIGURED/train)(.*?")', 'input_path: "{}"'.format(train_record_fname), s)
        if test:
            s = re.sub('(input_path: ".*?)(PATH_TO_BE_CONFIGURED/val)(.*?")', 'input_path: "{}"'.format(test_record_fname), s)
        else:
            s = re.sub('(input_path: ".*?)(PATH_TO_BE_CONFIGURED/val)(.*?")', 'input_path: "{}"'.format(val_record_fname), s)

        # label_map_path
        s = re.sub('label_map_path: ".*?"', 'label_map_path: "{}"'.format(label_map_path), s)

        # Set training batch_size.
        s = re.sub("batch_size: [0-9]+", "batch_size: {}".format(BATCH_SIZE), s)

        # Set training steps, num_steps
        s = re.sub("num_steps: [0-9]+", "num_steps: {}".format(NUM_STEPS), s)
        s = re.sub("total_steps: [0-9]+", "total_steps: {}".format(NUM_STEPS), s)
        # Set number of classes num_classes.
        s = re.sub("num_classes: [0-9]+", "num_classes: {}".format(num_classes), s)

        s = re.sub("learning_rate_base: 8e-2", "learning_rate_base: {}".format(LEARNING_RATE_BASE), s)
        s = re.sub("warmup_learning_rate: .001", "warmup_learning_rate: {}".format(WARMUP_LEARNING_RATE), s)

        # fine-tune checkpoint type
        s = re.sub('fine_tune_checkpoint_type: "classification"', 'fine_tune_checkpoint_type: "{}"'.format("detection"), s)

        # remove data augmentation
        s = re.sub(
            "data_augmentation_options {\n    random_horizontal_flip {\n    }\n  }\n  data_augmentation_options {\n    random_scale_crop_and_pad_to_square {\n      output_size: 512\n      scale_min: 0.1\n      scale_max: 2.0\n    }\n  }\n  optimizer",
            "optimizer",
            s,
        )

        f.write(s)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-p",
        "--path",
        type=str,
        help="path to dataset",
    )
    parser.add_argument(
        "-e",
        "--export",
        type=bool,
        help="export trained model",
    )
    args = vars(parser.parse_args())
    dataset_name = args["path"]
    label_map_path = os.path.join(ROOT_FOLDER, "data", dataset_name, "annotations", "label_map.pbtxt")

    if not os.path.isdir(MODEL_FOLDER):
        # PRETRAINED MODEL FOLDER
        download_tar = "http://download.tensorflow.org/models/object_detection/tf2/20200711/" + pretrained_checkpoint
        wget.download(download_tar, os.path.join(MODELS_FOLDER, pretrained_checkpoint))
        tar = tarfile.open(pretrained_checkpoint)
        tar.extractall(path=MODEL_FOLDER)
        tar.close()
    # PIPELINE CONFIG FILE
    wget.download(download_config, base_pipeline_config_path)

    num_classes = get_num_classes(label_map_path)
    # DEPLOY FOLDER
    os.chdir(DEPLOY_FOLDER)
    # Write custom pipeline config file
    write_custom_config_file(base_pipeline_config_path, custom_pipeline_config_path, dataset_name, test=True)
    OUTPUT_FOLDER = os.path.join(DEPLOY_FOLDER, model_name + "-output")

    os.system(
        "python3 model_main_tf2.py --alsologtostderr --model_dir={} --checkpoint_every_n={} --pipeline_config_path={} --eval_on_train_data 2>&1 | tee {}/train.log".format(
            OUTPUT_FOLDER, CHECKPOINT_INTERVAL, custom_pipeline_config_path, OUTPUT_FOLDER
        )
    )

    if args["export"]:
        EXPORT_FOLDER = os.path.join(DEPLOY_FOLDER, model_name + "-export")
        if not os.path.isdir(EXPORT_FOLDER):
            os.makedirs(EXPORT_FOLDER)
        os.system(
            "python3 exporter_main_v2.py --input_type='image_tensor' --pipeline_config_path={} --trained_checkpoint_dir={} --output_directory={}".format(
                custom_pipeline_config_path, OUTPUT_FOLDER, EXPORT_FOLDER
            )
        )
