#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 10 15:42:19 2020

@author: gaspard
"""

import glob
import json
import argparse

def clip1(x):
    return max(0,min(x,510))

def clip2(x):
    return max(1,min(x,511))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-f","--folder",type=str, help = "folder of images and labels")
    parser.add_argument("-o","---output", type=str,help = "output csv file")
    args = parser.parse_args()

    folder = args.folder
    output = args.output
    
    classes = "../labels_index.json"
    
    j = open("../labels_index.json")
    classes = json.load(j)
    j.close()
    
    files = glob.glob(folder+"*.jpg")
    
    f = open(output,"w")
    
    for file in files:
        label = file.replace("jpg","txt")
        g = open(label,"r")
        lines = g.readlines()
        g.close()
        
        if lines != []:
            box = lines[0].split(" ")
            cat,x,y,w,h =int(box[0]), float(box[1]), float(box[2]), float(box[3]), float(box[4])
            class_name = classes[str(cat)]
            new_box = [file,clip1(int((x-w/2)*512)),clip1(int((y-h/2)*512)),clip2(int((x+w/2)*512)),clip2(int((y+h/2)*511)),class_name]
            l = ",".join([str(x) for x in new_box])+"\n"
            
            f.write(l)
    
    f.close()
    
    f = open("class.csv","w")
    for k in classes.keys():
        f.write(f"{classes[k]},{k}\n")
    f.close()