# DL_Project_MVA

### Report [report.pdf](report.pdf)

### Video : [https://youtu.be/fo-5Mgmwstc](https://youtu.be/fo-5Mgmwstc)

### Slides : [slides.pdf](slides.pdf)

# Code

## Dataset Preparation

### Merge

Le script `merged_raw_datasets.py` permet de construire le dataset aggrégé avec des labels .txt au format YOLO à partir de différents datasets. 
Pour utiliser : 

```bash
python merge_raw_datasets.py -p dataset_path1 dataset_path2 
```

Par exemple:

```bash
python merge_raw_datasets.py -p ./data/asl-alphabet ./data/asl-alphabet-test ./data/ASL_Roboflow 
```

### Resize

Le script `resize_dataset.py` permet de resize notre dataset mergé vers une taille cible, pour utiliser : 

```bash
python resize_dataset.py -f folder -s size
```

Par exemple:

```bash
python resize_dataset.py -f "merged_dataset/data/" -s "512x512"
```

### Split

`split.py` permet de faire un split pré-defini a notre dataset : 

- ASLTest est divisé en deux pour test et val.
- Roboflow : 1/2 Train et 1/4 test/val
- ASL : 1/10 au training, le reste est écarté pour l'instant

Pour utiliser : 

```bash
python split.py -f folder -t target_folder
```

Par exemple:

```bash
python split.py -f "merged_dataset/" -t "splited_dataset/"
```



### Full Pipeline

Uses the different script in this order : 

- Merge
- Hand detection
- Resize
- Split

## Hand-Detector

Pré-requis :

- installer [keras-retinanet](https://github.com/fizyr/keras-retinanet)
- Télecharger [hand_detector_inf.h5](https://drive.google.com/file/d/1wIdMGICfQ3grzRAfHcSc6mahN9MApRe7/view?usp=sharing)

Le script `detect_to_yolo.py` permet de detecter les mains et de créer les labels au format YOLO. 
Pour utiliser : 

```bash
python detect_to_yolo.py -f files_path -m model_path -t threshold -o output_folder
```

Par exemple:

```bash
python detect_to_yolo.py -f "merged_dataset/data/ASL*.jpg" -m "./hand_detector_inf.h5" -t 0.5 -o "merged_dataset/labels/"
```



## Utils

### Show YOLO Labels

Le script `show_yolo_label.py` permet d'afficher les labels YOLO superposée a l'image. 
Appuyer sur les fleches de droite et gauche pour naviguer, Echap pour quitter. 

Pour utiliser : 

```bash
python show_yolo_label.py -i image_folder -l label_folder
```

Ne pas specifier `label_folder` si c'est le meme que `image_folder`.

## Training

### RetinaNet

1) Clone and install this repositery : [keras-retinanet](https://github.com/fizyr/keras-retinanet) and download [resnet50_coco_best_v2.1.0.h5](https://github.com/fizyr/keras-retinanet/releases/download/0.5.1/resnet50_coco_best_v2.1.0.h5)

2) Use script `make_csv.py` to create train.csv, test.csv and val.csv. 

```bash
python make_csv.py -f "../splited_dataset/test/" -o "test.csv"
```

3) Start the training with `train.sh`

```bash
retinanet-train --weights resnet50_coco_best_v2.1.0.h5 --snapshot-path snapshot/ --batch-size 4 --steps 2000 --epochs 10 --image-max-side 512 --tensorboard-dir ./logs/ csv train.csv class.csv --val-annotations val.csv 
```

4) [Optional] : Evaluation

Tensorboard for training metrics : loss and mAP. 

```bash
tensorboard --logdir=logs
```

### YOLOv4

1) Clone and install this repo : [darknet](https://github.com/AlexeyAB/darknet) and download [yolov4.weights](https://github.com/AlexeyAB/darknet/releases/download/darknet_yolo_v3_optimal/yolov4.weights)

2) Use script `make_txt.py` to create train.csv, test.csv and val.csv. 

```bash
python make_txt.py -f "../splited_dataset/test/" -o "test.txt"
```

3) Start the training with `train.sh`

```bash
../../darknet/darknet detector train obj.data yolo-obj.cfg yolov4.conv.137 -map
```

4)  [Optional] Evaluation : 

Open `chart.png` for loss and mAP

### EfficientDet

#### Getting started

Install Google Protocol Buffers [protobuf](https://developers.google.com/protocol-buffers) via 

Mac OS
```
brew install protobuf
```
Linux
```
apt install -y protobuf-compiler
```
If you have issues on installing protobuf, check that [issue](https://github.com/tensorflow/models/issues/1834) 


##### Install the Object Detection API from Google

```bash
bash ./efficientdet/scripts/build_object_detect_api.sh
```
#### Convert from YOLO labels to COCO JSON label files

```bash
python ./dataset_preparation/convert_yolo2coco.py -p path/to/dataset/folder -s train val test
```

##### Create TF Records from a dataset folder named `dataset_name` located in `data/` (e.g. here we have `splited_dataset`, `splited_dataset_aug` and `splited_dataset_aug_bg`)

```bash
bash ./efficientdet/scripts/create_tf_records.sh -d dataset_name
```

##### Create `labelmap.pbtxt` file from the label JSON file encoding

```bash
python ./efficientdet/scripts/create_label_map.py -p path/to/dataset/folder -l path/to/label/json
```

#### Training (without eval)

**If you want to export your model, before training you need to change manually a file in tensorflow package. Check this [issue](https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/issues.html#export-error).**

```bash
python ./efficientdet/scripts/train_script.py -p path/to/dataset/folder -e True
```
Change -e argument to False if you do not want to export the trained model.

If you want your training to perform evaluation in parallel (with tensorboard vizualizations), you will need to run on 2/3 different windows simultaneously (using `screen` or `tmux` for example).

1) Training script
```bash
python ./efficientdet/scripts/train_script.py -p path/to/dataset/folder -e True
```
2) Evaluation script
```bash
python ./efficientdet/scripts/eval_script.py
```
3) [OPTIONAL] Tensorboard vizualization
```bash
bash ./efficientdet/scripts/eval_tensorboard.sh -l ./efficientdet/deploy/models/efficientdet_d0_coco17_tpu-32-output/
```
## Inference

### RetinaNet

Use `video.py` and `webcam.py` for inference

### YOLOv4

Webcam :

```bash
../../darknet/darknet detector demo obj.data yolo-obj.cfg backup/yolo-obj_best.weights -c 0
```

Video

```bash
../../darknet/darknet detector demo obj.data yolo-obj.cfg backup/yolo-obj_best.weights test.mp4
```

### EfficientDet


#### From images folder
```bash
python ./efficientdet/detection/detect_objects.py --threshold 0.5 \
        --images_dir path/to/image/folder \
        --model_path ./efficientdet/deploy/models/efficientdet_d0_coco17_tpu-32/saved_model \
        --path_to_labelmap path/to/labelmap.pbtxt \
        --output_directory path/to/output/folder \
        --save_output
```
#### From video folder
```bash
python ./efficientdet/detection/detect_objects.py --threshold 0.5 \
        --video_input \
        --video_path path/to/video \
        --model_path ./efficientdet/deploy/models/efficientdet_d0_coco17_tpu-32/saved_model \
        --path_to_labelmap path/to/labelmap.pbtxt \
        --output_directory path/to/output/folder \
        --save_output
```
#### From integrated camera
```bash
python ./efficientdet/detection/detect_objects.py --threshold 0.5 \
        --video_input \
        --video_from_camera True \
        --model_path ./efficientdet/deploy/models/efficientdet_d0_coco17_tpu-32/saved_model \
        --path_to_labelmap path/to/labelmap.pbtxt \
        --output_directory path/to/output/folder \
        --save_output
```
