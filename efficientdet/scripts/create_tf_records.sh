
#while getopts d: flag
#do
#    case "${flag}" in
#        d) dataset_name=${OPTARG};;
#    esac
#done

if [ "$1" != "-d" ]; then 
    echo "[ERROR] Missing argument flag -d for dataset name."
    exit 1
elif [ "$2" == "" ]; then 
    echo "[ERROR] Missing argument after -d flag for dataset name."
    exit 1
fi

dataset_name=$2

cd ./efficientdet/models/research

mkdir -p ../../../data/$dataset_name/tf_records/

python3 object_detection/dataset_tools/create_coco_tf_record.py --logtostderr \
      --train_image_dir="../../../data/$dataset_name/train/" \
      --val_image_dir="../../../data/$dataset_name/val/" \
      --test_image_dir="../../../data/$dataset_name/test/" \
      --train_annotations_file="../../../data/$dataset_name/annotations/instances_train.json" \
      --val_annotations_file="../../../data/$dataset_name/annotations/instances_val.json" \
      --testdev_annotations_file="../../../data/$dataset_name/annotations/instances_test.json" \
      --output_dir="../../../data/$dataset_name/tf_records/"
