if [ "$1" != "-d" ]; then 
    echo "[ERROR] Missing argument flag -d for logs directory name."
    exit 1
elif [ "$2" == "" ]; then 
    echo "[ERROR] Missing argument after -d flag for logs directory name."
    exit 1
fi

log_folder=$2
tensorboard --logdir $log_folder