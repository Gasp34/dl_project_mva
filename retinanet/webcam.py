#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 13 15:06:45 2020

@author: gaspard
"""

import numpy as np
import cv2
import json
from keras_retinanet import models
from keras_retinanet.utils.image import read_image_bgr, preprocess_image, resize_image
from keras_retinanet.utils.visualization import draw_box
from keras_retinanet.utils.colors import label_color
import time

def draw_caption(image, box, caption):
    """ Draws a caption above the box in an image.

    # Arguments
        image   : The image to draw on.
        box     : A list of 4 elements (x1, y1, x2, y2).
        caption : String containing the text to draw.
    """
    b = np.array(box).astype(int)
    cv2.putText(image, caption, (b[0], b[1] - 10), cv2.FONT_HERSHEY_PLAIN, 2, (0, 0, 0), 2)
    cv2.putText(image, caption, (b[0], b[1] - 10), cv2.FONT_HERSHEY_PLAIN, 2, (255, 255, 255), 1)

j = open("../labels_index.json")
classes = json.load(j)
j.close()

model = models.load_model("snapshot_aug_bg/resnet50_csv_02.h5", backbone_name='resnet50')
model = models.convert_model(model)

def draw_frame(frame):
    # copy to draw on
    draw = frame.copy()
    
    # preprocess image for network
    frame = preprocess_image(frame)
    frame, scale = resize_image(frame,max_side=512)
    
    # process image
    start = time.time()
    boxes, scores, labels = model.predict_on_batch(np.expand_dims(frame, axis=0))
    end = time.time() - start
    
    # correct for image scale
    boxes /= scale
    
    # visualize detections
    box, score, label = boxes[0][0], scores[0][0], labels[0][0]
    # scores are sorted so we can break
    if score > 0.1:
        color = label_color(label)
    
        b = box.astype(int)
        draw_box(draw, b, color=color)
    
        caption = "{} {:.3f}".format(classes[str(label)], score)
        draw_caption(draw, b, caption)
    
    return draw


cap = cv2.VideoCapture(0)
while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Our operations on the frame come here
    draw = draw_frame(frame)

    # Display the resulting frame
    cv2.imshow('frame',draw)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()