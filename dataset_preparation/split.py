#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  5 12:29:19 2020

@author: gaspard
"""

import glob
import os
import shutil
import argparse

def make_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-f","--folder",type=str, help = "folder to split")
    parser.add_argument("-t","--target",type=str, help = "target folder of the split")
    args = parser.parse_args()

    data_path = args.folder
    target_path = args.target
    
    files = glob.glob(data_path+"data/*.jpg")
    files.sort() 
    
    make_dir(target_path+"train/")
    make_dir(target_path+"test/")
    make_dir(target_path+"val/")
    
    for i,file in enumerate(files):
        file_split = file.split("/")
        label = file.replace("/data","/labels").replace("jpg","txt")
        label_split = label.split("/")
        if "ASLTest" in file: #split is half to test half to val
            if i%2 == 0:
                new_path = target_path+"val/"+file_split[-1]
                new_label = target_path+"val/"+label_split[-1]
            else:
                new_path = target_path+"test/"+file_split[-1]
                new_label = target_path+"test/"+label_split[-1]
                    
        elif "Roboflow" in file: #split is half to train, quarter to test and val
            if i%4 <= 1:
                new_path = target_path+"train/"+file_split[-1]
                new_label = target_path+"train/"+label_split[-1]
            elif i%4 == 2:
                new_path = target_path+"val/"+file_split[-1]
                new_label = target_path+"val/"+label_split[-1]
            elif i%4 == 3:
                new_path = target_path+"test/"+file_split[-1]
                new_label = target_path+"test/"+label_split[-1]
                
        elif "ASL" in file: #split is one out of 10 in training
            if i%10 == 0:
                new_path = target_path+"train/"+file_split[-1]
                new_label = target_path+"train/"+label_split[-1]
        shutil.copy2(file, new_path)
        try:
            shutil.copy2(label, new_label)
        except:
            f = open(new_label,"w")
            f.close()
