import os
import imgaug as ia
import imgaug.augmenters as iaa
from imgaug.augmentables.bbs import BoundingBox, BoundingBoxesOnImage
from multiprocessing.pool import Pool
from functools import partial

import cv2

import json
import pandas as pd
import argparse
import numpy as np
from tqdm import tqdm
from collections import defaultdict
import glob
import random

SEED  = 12345
ia.seed(SEED)

WORKERS = 4
N_AUGMENT_PER_FILE = 5

IMAGE_WIDTH = 512
IMAGE_HEIGTH = 512


augment_blur = iaa.OneOf([
        iaa.GaussianBlur((0, 1.0)),
        iaa.AverageBlur(k=(2, 3)),
        iaa.MedianBlur(k=(3, 3)),
                ])
augment_noise = iaa.OneOf([
        iaa.AdditiveGaussianNoise(
                    loc=0, scale=(0.0, 0.05*255), per_channel=0.5
                ),
        iaa.Add((-10, 10), per_channel=0.5),
    ])
augment_contrast = iaa.OneOf([
        iaa.Multiply((0.5, 1.5)),
        iaa.LinearContrast((0.5, 2.0)),
    ])
augment_affine = iaa.Affine(
            scale={"x": (0.8, 2), "y": (0.8, 2)},
            translate_percent={"x": (-0.30, 0.30), "y": (-0.30, 0.30)},
            rotate=(-15, 15),
            shear=(-16, 16),
            order=[0, 1],
            cval=0,
            mode=ia.ALL
        )

augmenters = iaa.Sequential([
        augment_blur,
        augment_noise,
        augment_contrast,
        augment_affine,
        ],
        random_order=True)

def make_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

def read_file_to_ia_format(image_filepath):
    label_filepath = image_filepath[:-3] + 'txt'
    with open(label_filepath, 'r') as file:
        lines = file.readlines()
        label, x_center, y_center, width, heigth = lines[0].split(' ')

    x1 = int((float(x_center) - float(width)/2) * IMAGE_WIDTH)
    y1 = int((float(y_center) - float(heigth)/2) * IMAGE_HEIGTH)
    x2 = int((float(x_center) + float(width)/2) * IMAGE_WIDTH)
    y2 = int((float(y_center) + float(heigth)/2) * IMAGE_HEIGTH)

    image = cv2.imread(image_filepath)
    bbs = BoundingBoxesOnImage([
        BoundingBox(x1=x1, y1=y1, x2=x2, y2=y2)
    ], shape=image.shape)

    return image, label, bbs

def load_augmenters():
    return augmenters

def write_augmented_files(augmented_images, target_path):
    for image_path, augmented_images_list in augmented_images.items():
        for augment_index, augmented_img_dict in enumerate(augmented_images_list):
            image_filename = image_path.split('/')[-1]
            augmented_filename = image_filename.split('.')[0] + "_" + str(augment_index)
            new_img_path = target_path + augmented_filename + '.jpg'
            new_label_path = target_path + augmented_filename + '.txt'

            with open(new_label_path, 'w') as label_file:
                x1, y1, x2, y2 = augmented_img_dict["bbs"]
                x_center = (x1 + x2) / 2 / IMAGE_WIDTH
                y_center = (y1 + y2) / 2 / IMAGE_HEIGTH
                width = (x2 - x1) / IMAGE_WIDTH
                heigth = (y2 - y1) / IMAGE_HEIGTH
                label_file.write("{} {:.4f} {:.4f} {:.4f} {:.4f}".format(augmented_img_dict["label"], x_center, y_center, width, heigth))
            cv2.imwrite(new_img_path,augmented_img_dict["image"])


def augment_images(image_files,bg=False):
    return {img_file: augment_image(img_file,bg) for img_file in image_files}

def augment_image(img_file,bg):
    image, label, bbs = read_file_to_ia_format(img_file)
    augmented_images = []
    
    true_image = image.copy()
    for i in range(N_AUGMENT_PER_FILE):
        if bg:
            i = random.randint(0,len(coco)-1)
            coco_image  = cv2.imread(coco[i])
            coco_image = cv2.resize(coco_image, (IMAGE_WIDTH,IMAGE_HEIGTH))
            coco_image[159:353,159:353,:] = true_image[159:353,159:353,:]
            image = coco_image
        
        image_aug, bbs_aug = augmenters(image=image, bounding_boxes=bbs)
        augmented_images.append({"label": label,
        "image": image_aug,
        "bbs": (bbs_aug.bounding_boxes[0].x1, bbs_aug.bounding_boxes[0].y1, bbs_aug.bounding_boxes[0].x2, bbs_aug.bounding_boxes[0].y2)
        })
    return augmented_images


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-f","--folder",type=str, help = "dataset folder to augment")
    parser.add_argument("-t","--target",type=str, help = "target folder of the augmented split dataset")
    parser.add_argument("-bg","--background", help = "use COCO image for background", action="store_true")
    args = parser.parse_args()

    data_path = args.folder
    target_path = args.target
    folder_name = data_path.split('/')[-1]
    bg = args.background
    
    files = glob.glob(data_path+"/*.jpg")
    files.sort() 
    
    coco = glob.glob("../../coco2017/*.jpg")
    
    make_dir(target_path)
    
    augmenters = load_augmenters()
    augmented_images = {}
    print('Augment files')
    #images_splitted = np.array_split(files, WORKERS)
    #with Pool(WORKERS) as pool:
    #    splitted_augmented_images = list(pool.map(augment_images, images_splitted))
    
    batch = 16
    for i in tqdm(range(len(files)//batch + 1)):
        files_batch = files[i*batch:(i+1)*batch]
        
        augmented_images = augment_images(files_batch,bg)
        #with Pool(WORKERS) as pool:
        #    list(pool.map(partial(write_augmented_files, target_path=target_path), splitted_augmented_images))
        write_augmented_files(augmented_images, target_path)
    